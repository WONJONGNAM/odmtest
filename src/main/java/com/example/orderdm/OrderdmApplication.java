package com.example.orderdm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderdmApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderdmApplication.class, args);
    }

}
