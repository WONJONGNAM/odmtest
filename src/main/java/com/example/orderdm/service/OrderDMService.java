package com.example.orderdm.service;

import com.example.orderdm.model.OrderDMModel;
import com.example.orderdm.util.HttpUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class OrderDMService {

    private final RestTemplate restTemplate;

    @Value("http://localhost:8090/omsSearch")
    private String omsApiUrl;

    public OrderDMService(RestTemplateBuilder restTemplateBuilder) {
        System.out.println("ㅇㅕ기타나??");
        this.restTemplate = restTemplateBuilder.build();
    }

    public String getOmsSearch(String orderNo) {
        System.out.println("omsApiUrl :: "+omsApiUrl);

        try{
            HttpHeaders httpHeaders = makeHeader();
           // HttpHeaders httpHeaders =HttpUtils.makeHeaders();

            System.out.println("orderNo :: "+orderNo);
            OrderDMModel orderDMModel = new OrderDMModel(orderNo);

            System.out.println("orderNo222 :: "+orderDMModel.getOrderNo());

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(omsApiUrl)
                    .queryParam("orderNo", orderNo);


            ResponseEntity<OrderDMModel> odm = restTemplate.exchange(
                    builder.toUriString()
                    , HttpMethod.GET
                    , HttpUtils.makeRequestEntity(orderDMModel,httpHeaders)
                    , OrderDMModel.class
                    );

            System.out.println("odm.toString() :: "+odm.toString());

            String zz = odm.getBody().getOrderNo();

            System.out.println("odm.getBody().getOrderNo() :: "+odm.getBody().getOrderNo());
            //1.VO로 받는거
            //2. List , Map , JSON

        } catch (HttpClientErrorException e) {
           e.printStackTrace();
        } catch (HttpServerErrorException e) {
          e.printStackTrace();
        }

        return "";
    }

    private HttpHeaders makeHeader(){
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return httpHeaders;
    }


}
