package com.example.orderdm.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.time.LocalDateTime;


@ToString
@Getter
@Setter
@NoArgsConstructor
public class OrderDMModel {
    private String orderNo;
    private String orderKind;




    @Builder
    public OrderDMModel(String orderNo) {
        this.orderNo = orderNo;

    }


}
